﻿using UnityEngine;
using System.Collections;
using System;

public class AscenseurController : MonoBehaviour {

	public float force;
	public float augmentation;
	public int min, max;
	public Transform topFloor;
	public float decalage;
    public Collider2D collidBlock;

	public Transform chariot;

	private bool isTopFloor;
	private bool isOnAsc;
	private bool isTrembling;
	private bool keyAPressed;
	private bool keyEPressed;

	private DateTime dateOld;

	private float gravity;
	private float firstYPosition;
	private Transform trans;

	private System.Random r;
	int getRandomNumber()
	{
		return r.Next(min, max);
	}

	// Use this for initialization
	void Start () {
		r = new System.Random ();

		dateOld = DateTime.Now;

		trans = this.transform;
		firstYPosition = trans.position.y;
		gravity = 0f;

		isTopFloor = false;
		isOnAsc = false;
		keyEPressed = false;
		keyAPressed = false;
		isTrembling = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.tag == "Player")
		{
			isOnAsc = true;
            collidBlock.enabled = true;
        }
	}

	void OnCollisionExit2D(Collision2D coll)
	{
        if (coll.gameObject.tag == "Player")
        {
            isOnAsc = false;
            collidBlock.enabled = true;
        }
    }

	void FixedUpdate () {


		if (isOnAsc && this.transform.position.y < topFloor.position.y) {		

			if (TestLeftAction() && !keyAPressed) {
				gravity = 0f;
				keyEPressed = false;
				keyAPressed = true;

				trans = this.transform;
				this.transform.position = new Vector3 (trans.position.x, trans.position.y + force, trans.position.z);

				isItShaking();
			} else if (TestRightAction() && !keyEPressed) {
				gravity = 0f;
				keyAPressed = false;
				keyEPressed = true;

				trans = this.transform;
				this.transform.position = new Vector3 (trans.position.x, trans.position.y + force, trans.position.z);

				isItShaking();
			} else if ( TestBothAction()) {
				gravity = 0f;
			} else if (this.transform.position.y > firstYPosition && !isTopFloor) {
				trans = this.transform;
				this.transform.position = new Vector3 (trans.position.x, trans.position.y - gravity, trans.position.z);
				gravity += augmentation;
			}

			Shake();
		}
	}

	void isItShaking(){		
		if (isTrembling) {
			print ("Debug : isItShaking enter ");
			chariot.position = new Vector3 (chariot.position.x - decalage, chariot.position.y, chariot.position.z);
		}
	}

	void Shake()
	{
		DateTime now = DateTime.Now;
		TimeSpan diff = now.Subtract (dateOld);

		int rng = getRandomNumber ();

		if (diff.TotalSeconds > 2)
			isTrembling = false;

		if (diff.TotalSeconds > rng) {
			isTrembling = true;
			EventHandler.TriggerEvent (EventHandler.GameEvent.MakeShake);
			dateOld = now;
		}




	}

	bool TestLeftAction()
	{
		if (Input.GetKeyUp (KeyCode.A) || Input.GetKeyUp (KeyCode.JoystickButton4))
			return true;
		return false;
	}

	bool TestRightAction()
	{
		if (Input.GetKeyUp (KeyCode.E) || Input.GetKeyUp (KeyCode.JoystickButton5))
			return true;
		return false;
	}

	bool TestBothAction()
	{
		if ((Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.JoystickButton4)) &&
		    (Input.GetKey (KeyCode.E) || Input.GetKey (KeyCode.JoystickButton5)))
			return true;
		return false;
	}



}



