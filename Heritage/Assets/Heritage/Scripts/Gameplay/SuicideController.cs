﻿using UnityEngine;
using System.Collections;

public class SuicideController : MonoBehaviour {

	public Rigidbody2D chariot;
	public Camera camera;

	private PlayerController controleur;
	private GameObject scene;


	// Use this for initialization
	void Start () {
		scene = this.transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player") {
			scene.GetComponent<MiniScene> ().DesactivateController ();
			controleur = scene.GetComponent<MiniScene> ().controller;
			controleur.GetComponent<DistanceJoint2D> ().enabled = false;
			controleur.GetComponent<Animator> ().enabled = false;

			print ("Debug : enter ontrigger");
			if (col.gameObject.tag == "Player" && Chariot.hasFail == true)
				animateSuicide ();
			else 
				animateChariotThrow ();
		}

	}

	void animateSuicide ()
	{
		
	}

	void animateChariotThrow ()
	{
		
		print ("Debug : enter throw");
		chariot.velocity = new Vector2 (1.5f, chariot.velocity.y);
		print ("Chariot vitesse x : " + chariot.velocity.x);
		if (scene.name.Contains ("gen3")) {
			camera.GetComponent<CamCenter> ().follow = scene.transform.Find ("traineau");
			scene.transform.Find ("ChariotTrigger").gameObject.SetActive (false);
		}

	}
}
