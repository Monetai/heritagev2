﻿using UnityEngine;
using System.Collections;

public class SpriteTexSinAlphaMovement : MonoBehaviour {

	public float speed = 1.0f;
	public float intensity = 1.0f;

	private Renderer render;

	void Start()
	{
		render = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float time = Time.timeSinceLevelLoad;
		render.material.SetTextureOffset("_AlphaTex", new Vector2(Mathf.Sin(time * speed) * intensity, 0));
	}
}
