﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerBruitage : MonoBehaviour {

	public AudioSource source;
	public List<AudioClip> bruitages;

	public void PlayRandom()
	{
		if (!source.isPlaying) 
		{
			source.PlayOneShot(bruitages[Random.Range(0,bruitages.Count)]);
		}
	}
}
