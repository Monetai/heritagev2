﻿using UnityEngine;
using System.Collections;

public class ColorController : MonoBehaviour {

	public Camera camera;

	private Color oldColor;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		oldColor = camera.backgroundColor;
		camera.backgroundColor = new Color(0.69f, 0.72f, 0.73f, 0.02f);
	}

	void OnDisable()
	{
		camera.backgroundColor = oldColor;
	}

}
