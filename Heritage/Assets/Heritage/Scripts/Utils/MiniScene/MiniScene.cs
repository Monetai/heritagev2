﻿using UnityEngine;
using System.Collections;

public class MiniScene : MonoBehaviour {


	public FadingType color;
	public PlayerController controller;


	void Start()
	{
		EventHandler.AddEventListener(EventHandler.GameEvent.OnChangeScene, DesactivateController);
		EventHandler.AddEventListener(EventHandler.GameEvent.OnFadingInFinished, ActivateController);
		controller.enabled = false;
	}

	public void DesactivateController()
	{
		controller.enabled = false;
	}

	public void ActivateController()
	{
		controller.enabled = true;
	}

}
