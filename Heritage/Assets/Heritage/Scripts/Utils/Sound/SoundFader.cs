﻿using UnityEngine;
using System.Collections;

public class SoundFader : MonoBehaviour {

	public float speed;
	public AudioSource toSoundFader;

	bool isFading = false;
	// Update is called once per frame
	void FixedUpdate () {
		if (isFading) {
			toSoundFader.volume -= speed;
		}
	
	}

	void OnTriggerEnter2D(Collider2D col) {
		isFading = true;
	}
}
