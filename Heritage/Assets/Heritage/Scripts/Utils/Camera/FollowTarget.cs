﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {
	public Vector3 offset;
	public bool followY = false;
	public float smoothTime = 0.1f;

	private Vector3 speed = Vector3.zero;
	private GameObject target;
	
	void Awake () {
		target = GameObject.FindWithTag ("Player");
	}

	void LateUpdate () {
		if(target == null) {
			return;
		}

		Vector3 oldPos = transform.position;
		Vector3 newPos = target.transform.position + offset;

		if(!followY)
			newPos.y = oldPos.y;

		transform.position = Vector3.SmoothDamp(transform.position, newPos, ref speed, smoothTime);
	}
}
