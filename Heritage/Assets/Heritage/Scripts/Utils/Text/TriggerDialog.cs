﻿using UnityEngine;
using System.Collections;

public class TriggerDialog : MonoBehaviour {

	public bool onlyOnce = true;
	public Dialog dialogToStart;

	bool started = false;

	// Use this for initialization
    void OnTriggerEnter2D()
    {
		if (started && onlyOnce)
			return;

		if (!dialogToStart.isRunning ())
		{
			dialogToStart.startDialog ();
			started = true;
		}
    }
}
