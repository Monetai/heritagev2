﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System.Linq;

public class Dialog : MonoBehaviour {

	[System.Serializable]
	public class DialogSeq
	{
		public string msg;
		public float time;
		public Color col;
	}

	public Text txt;
	public Image backTxt;
	public DialogSeq[] dialog;

	public float timeBetweenEachLetter = 0.1f;
	public float fadingTime = 1.0f;

	private float timer;
	private int dialogIndex;
	private int letterIndex;
	private bool run;
	private bool inFading;

	private int fadeVal;

	bool textAnim;

	void fadeIn()
	{
		fadeVal = 1;
		inFading = true;
	}

	void fadeOut()
	{

		fadeVal = -1;
		inFading = true;
	}

	void fade()
	{
		if (!inFading)
			return;

		Color c = backTxt.color;
		c.a += fadeVal * Time.deltaTime / fadingTime;
		backTxt.color = c;

		if ((fadeVal == -1 && c.a <= 0) || (fadeVal == 1 && c.a >= 1)) {
			fadeVal = 0;
			inFading = false;
		}
	}

	// Use this for initialization
	void Start ()
	{
		run = false;
		backTxt.enabled = false;
		inFading = false;


		Color c = backTxt.color;
		c.a = 0;
		backTxt.color = c;

		if (dialog.Length == 0)
			startDialog ();
	}

    public bool isRunning()
    {
        return run;
    }

	public void startDialog()
	{
		run = true;
		textAnim = true;
		timer = 0;
		dialogIndex = 0;
		letterIndex = 0;
		//txt.text = dialog[0].msg;
		txt.text = "";
		txt.color = dialog [0].col;
		backTxt.enabled = true;
		inFading = true;
		fadeIn ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		fade ();

		if (run && !inFading)
		{
			DialogSeq currentSeq = dialog [dialogIndex];
			timer += Time.deltaTime;

			if (textAnim)
			{
				if (timer > timeBetweenEachLetter)
				{
					string targetText = currentSeq.msg;
					char newLetter = targetText [letterIndex];

					StringBuilder builder = new StringBuilder (txt.text);
					builder.Append (newLetter);
					txt.text = builder.ToString ();

					timer = 0;
					letterIndex++;
					if (letterIndex == targetText.Length) {
						textAnim = false;
						letterIndex = 0;
					}
				}

				}
			else if (timer > currentSeq.time)
			{
				timer = 0;
				textAnim = true;
				dialogIndex++;
				if (dialogIndex == dialog.Length)
				{
					run = false;
					txt.text = "";
					//backTxt.enabled = false;
					fadeOut();
				} else {
					//txt.text = dialog [dialogIndex].msg;
					txt.text = "";
					txt.color = dialog[dialogIndex].col;
				}
			}
		}
	}
}
