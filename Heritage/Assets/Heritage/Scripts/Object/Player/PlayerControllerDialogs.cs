﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System.Linq;

public enum Speaker
{
	Speaker_Father,
	Speaker_Child
}

[System.Serializable]
public class TextDialog
{
	public Color color;
	public string text;
	public Speaker speaker;
}

public class PlayerControllerDialogs : PlayerController {

	public TextDialog[] dialog;

	public float timeBetweenEachLetter = 0.1f;
	public float fadingTime = 1.0f;

	public Text dispText;
	public Image fadeBackText;

	public Text dispTextChild;
	public Image fadeBackTextChild;

	private int indexDialog;
	private int indexLetter;
	private float timer;
	private bool textAnim;
	private bool inFading;
	 
	int fadeVal = 0;
	int fadeValChild = 0;

	void Start()
	{
		indexDialog = -1;
		indexLetter = 0;
		timer = 0;

		textAnim = false;
		inFading = false;

		Color c = fadeBackText.color;
		c.a = 0;
		fadeBackText.color = c;

		c = fadeBackTextChild.color;
		c.a = 0;
		fadeBackTextChild.color = c;
	}
		
	void fadeIn(Speaker speaker)
	{
		if (speaker == Speaker.Speaker_Father)
			fadeVal = 1;
		else
			fadeValChild = 1;
		
		inFading = true;
	}

	void fadeOut(Speaker speaker)
	{
		if (speaker == Speaker.Speaker_Father)
			fadeVal = -1;
		else
			fadeValChild = -1;
		
		inFading = true;
	}

	void fade()
	{
		if (!inFading)
			return;

		Color c = fadeBackText.color;
		c.a += fadeVal * Time.deltaTime / fadingTime;
		fadeBackText.color = c;

		if ((fadeVal == -1 && c.a <= 0) || (fadeVal == 1 && c.a >= 1)) {
			fadeVal = 0;
			inFading = false;
		}


		c = fadeBackTextChild.color;
		c.a += fadeValChild * Time.deltaTime / fadingTime;
		fadeBackTextChild.color = c;

		if ((fadeValChild == -1 && c.a <= 0) || (fadeValChild == 1 && c.a >= 1)) {
			fadeValChild = 0;
			inFading = false;
		}
		else if (fadeValChild != 0)
			inFading = true;
	}

	bool isFadeIn(Speaker speaker)
	{
		if (speaker == Speaker.Speaker_Father)
			return fadeBackText.color.a >= 1;
		else
			return fadeBackTextChild.color.a >= 1;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// No dialog
		if (dialog.Length == 0) {
			EventHandler.TriggerEvent (EventHandler.GameEvent.OnChangeScene);
			return;
		}

		// fading of the background
		fade ();

		// the dialog is finished
		if (indexDialog == dialog.Length)
			return;
		
		if (textAnim)
		{
			if (inFading)
				return;
			
			timer += Time.deltaTime;
			string targetText = dialog[indexDialog].text;

			if (timer > timeBetweenEachLetter)
            {
                char newLetter = targetText[indexLetter];


				if (dialog [indexDialog].speaker == Speaker.Speaker_Father) {
					StringBuilder builder = new StringBuilder(dispText.text);
					builder.Append(newLetter);
					dispText.text = builder.ToString ();
					dispText.color = dialog [indexDialog].color;
				}
				else
				{
					StringBuilder builder = new StringBuilder(dispTextChild.text);
					builder.Append(newLetter);
					dispTextChild.text = builder.ToString ();
					dispTextChild.color = dialog [indexDialog].color;
				}

				timer = 0;
				indexLetter++;
				if (indexLetter == targetText.Length)
				{
					textAnim = false;
					indexLetter = 0;
				}

			}
		}
		if (Input.GetButtonDown("Fire1"))
		{
			// Complete the text if the player has skipped
			if (indexDialog >= 0)
			{
				if (dialog [indexDialog].speaker == Speaker.Speaker_Father)
					dispText.text = dialog [indexDialog].text;
				else
					dispTextChild.text = dialog [indexDialog].text;
			}


			textAnim = true;
			indexDialog++;
			indexLetter = 0;

			if (indexDialog == dialog.Length)
			{
				dispText.text = "";
				dispTextChild.text = "";
				fadeOut (Speaker.Speaker_Father);
				fadeOut (Speaker.Speaker_Child);
				EventHandler.TriggerEvent (EventHandler.GameEvent.OnChangeScene);
				return;
			}

			if (!isFadeIn (dialog [indexDialog].speaker))
				fadeIn (dialog [indexDialog].speaker);

			if (dialog[indexDialog].speaker == Speaker.Speaker_Father)
				dispText.text = "";
			else
				dispTextChild.text = "";
		}
	}
}
