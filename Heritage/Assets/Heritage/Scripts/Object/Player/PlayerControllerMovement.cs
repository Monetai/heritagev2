﻿using UnityEngine;
using System.Collections;

public class PlayerControllerMovement : PlayerController {

	// Acceleration
	public float speed = 1.0f;
	public float lowSpeed = 1.0f;

	// Max distance to take the chariot
	public float distToTake = 1.0f;

    public float distMaxToGoChariot;

	// Max speed
	public float maxSpeed;

	public Transform frontChariot;
	public Transform backChariot;
	private int selectedJoint;


	private Rigidbody2D rb;

	private Rigidbody2D rbChariot;
	private Collider2D collChariot;

	private DistanceJoint2D joint;

	private Animator anim;

	private bool inBoat;
    private bool inJump;


    public SpriteRenderer chariotSP;
	public Sprite playerChariotSprite;
	private Sprite initialSprite;

	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody2D> ();
		joint = GetComponent<DistanceJoint2D> ();
		anim = GetComponent<Animator> ();

		inBoat = false;
        inJump = false;

        selectedJoint = 0;

		rbChariot = joint.connectedBody;
		collChariot = rbChariot.gameObject.GetComponent<Collider2D> ();

		initialSprite = chariotSP.sprite;
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
        inJump = false;

		Debug.Log (coll.gameObject.name);
		if (coll.gameObject.name == "eauColl") {
			Debug.Log ("FAIL !!! ");
			EventHandler.TriggerEvent (EventHandler.GameEvent.OnPlayerFail);
			EventHandler.TriggerEvent (EventHandler.GameEvent.OnChangeScene);
		}
	}


	void Update()
	{
		if (Input.GetButton ("Fire1")
		    && Vector2.Distance (frontChariot.position, transform.position) < distToTake
		    && (transform.position - frontChariot.position).x > 0)
		{
			joint.connectedBody = rbChariot;
			joint.distance = Vector2.Distance (transform.position, rbChariot.transform.position);
			joint.enabled = true;
			selectedJoint = 1;
		}
		else if (Input.GetButton ("Fire1")
		         && Vector2.Distance (backChariot.position, transform.position) < distToTake
		         && (transform.position - frontChariot.position).x < 0)
		{
			joint.connectedBody = rbChariot;
			joint.distance = Vector2.Distance (transform.position, rbChariot.transform.position);
			joint.enabled = true;
			selectedJoint = -1;
		}
		else if (!Input.GetButton ("Fire1"))
		{
			joint.enabled = false;
			selectedJoint = 0;
		}

		if (Input.GetButtonDown ("Fire2") && Vector2.Distance(transform.position, chariotSP.transform.position) <= distMaxToGoChariot)
		{
			if (chariotSP.sprite == initialSprite) {
				chariotSP.sprite = playerChariotSprite;
				inBoat = true;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}
			else
			{
				chariotSP.sprite = initialSprite;
				inBoat = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = true;
                rb.AddForce(new Vector2(600, 1200));
                inJump = true;
            }

		}


	}

	void FixedUpdate ()
	{
		float speedUsed = speed;
		// Player movement
		if (Input.GetKey (KeyCode.LeftShift))
			speedUsed = lowSpeed;

		float horiz = Input.GetAxis ("Horizontal");

        if (!inJump)
        {
            if (inBoat)
                rbChariot.velocity = new Vector2(horiz, 0) * lowSpeed;

            //rb.AddForce (new Vector2 (horiz, 0) * speed, ForceMode2D.Force);
            rb.velocity = new Vector2(horiz, 0) * speedUsed;
        }

		//if (rb.velocity.magnitude > maxSpeed)
		//	rb.velocity = rb.velocity.normalized * maxSpeed;

		float direction = rb.velocity.x / Mathf.Abs (rb.velocity.x);


		anim.SetFloat ("speed", rb.velocity.magnitude / 5.0f);

		if (!joint.enabled)
		{
			anim.SetBool ("push", false);
			anim.SetBool ("pull", false);
		}
		else if ((direction == -1 && selectedJoint == -1) || (direction == 1 && selectedJoint == 1))
		{
			anim.SetBool ("pull", true);
			anim.SetBool ("push", false);
		}
		else
		{
			anim.SetBool ("pull", false);
			anim.SetBool ("push", true);
		}
			


		if (rb.velocity.x < 0)
			gameObject.GetComponent<SpriteRenderer> ().flipX = true;
		else
			gameObject.GetComponent<SpriteRenderer> ().flipX = false;
		//Debug.Log (rb.velocity);

		if (inBoat)
			transform.position = collChariot.transform.position;
	}

}
